# Markdown formula

Math written in LaTeX syntax is rendered with [KaTeX](https://github.com/KaTeX/KaTeX)

Some examples of ways in which math can be expressed in markdown.

$$e^{i \pi} + 1 = 0$$

The area of a circle is $A = \pi r^2$.

$`A = \pi r^2`$.

```math
e^{i \pi} + 1 = 0
```

Reference is made to Gitlab [docs](https://docs.gitlab.com/ee/user/markdown.html#math) 
